﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_Form
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void subBtn_Click(object sender, EventArgs e)
        {
            string result = myChoice(selectBox.Text.ToLower());

            printTxt.Visible = true;
            printTxt.Text = result;
        }


        /*-- Program Logic --*/

        static string myChoice(string choice)
        {
            string retort;

            switch (choice)
            {
                case "vegetables":

                    retort = "You're Healthy!";
                    return retort;

                case "fruit":

                    retort = "You're healthy! You just need to cut back on those sugars.";
                    return retort;

                case "candy":

                    retort = "You're being unhealthy!";
                    return retort;

                default:

                    retort = "Incorrect!";
                    return retort;

            }
        }



        /*-- End Logic --*/
    }
}
