﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to my amazing Distance Converter(Km & Miles)\n");
            Console.WriteLine("Please Select the type of metric you want to convert(KM or Miles)");
            string choice = Console.ReadLine().ToLower();
            Console.Write("\nType the amount of {0} you want to convert: ", choice);
            double value = Convert.ToDouble(Console.ReadLine());
            string converted = theChosenOne(choice, value);
            Console.WriteLine("\n{0}", converted);


        }

        /*-- Program Logic --*/

        static string theChosenOne(string choice, double value)
        {

            string converted;

            switch (choice)
            {
                case "miles":

                    converted = convertMile(value).ToString("#.##KM");
                    return converted;

                case "km":

                    converted = convertKm(value).ToString("#.## Miles");
                    return converted;

                default:

                    return null;
            }
        }




        static double convertKm(double mile)
        {
            double convertedKm = mile * 0.62137119;

            return convertedKm;

        }

        static double convertMile(double km)
        {
            double convertedMile = km / 0.62137119;

            return convertedMile;

        }

        /*-- End Logic --*/

    }
}
