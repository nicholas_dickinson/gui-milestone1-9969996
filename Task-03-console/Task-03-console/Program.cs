﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to my Choices Application! \n\nPlease Select an Option by typing the word associated with the number:\n");

            Console.WriteLine(" 1.Vegetables\n 2.Fruit\n 3.Candy\n");

            Console.Write("\nSelected Option: ");
            string choice = Console.ReadLine().ToLower();

            string retort = myChoice(choice);
            Console.WriteLine("\n{0}", retort);

        }

        /*-- Program Logic --*/

        static string myChoice(string choice)
        {
            string retort;

            switch (choice)
            {
                case "vegetables":

                    retort = "You're Healthy!";
                    return retort;

                case "fruit":

                    retort = "You're healthy! Sort of... you just need to cut back on those sugars.";
                    return retort;

                case "candy":

                    retort = "You're being unhealthy!";
                    return retort;

                default:

                    retort = "Incorrect!";
                    return retort;

            }
        }

        /*-- End Logic --*/
    }
}
