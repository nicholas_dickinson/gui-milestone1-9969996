﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Welcome to yet another Guessing Game! Fruit & Vegetable Edition!\n");
            Console.Write("\nPlease Input a fruit or vegetable: ");
            string guess = Console.ReadLine().ToLower();

            string answer = dictionary(guess);

            Console.WriteLine("\n{0}", answer);

        }

        /*-- Program Logic --*/

        static string dictionary(string guess)
        {
            
            Dictionary<string, string> newdiction = new Dictionary<string, string>();

            newdiction.Add("apple", "Fruit");
            newdiction.Add("banana", "Fruit");
            newdiction.Add("pear", "Fruit");
            newdiction.Add("tomato", "vegetable");
            newdiction.Add("cucumber", "vegetable");
            newdiction.Add("potato", "vegetable");

            string answer;


                if (newdiction.ContainsKey(guess))
                {

                    answer = "Your Guess is Correct!";
                    return answer;

                }
                else {

                    answer = "Wrong! Better luck next time!";
                    return answer;
                }
            

        }

         }

        /*-- End Logic --*/

    }



          


