﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02_Console2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> listD = new List<double>();
            int count = 0;
            int amount;

            Console.Write("Please enter the Amount of items you want to add together: ");
            amount = Convert.ToInt32(Console.ReadLine());

            while(count < amount)
            {
                Console.Write("\nPlease Enter a value:");
                double addition = Convert.ToDouble(Console.ReadLine());
                listD.Add(addition);
                count++;
            }

            Console.WriteLine("\nYour total is: {0}", calculate(listD));

        }


        static string calculate(List<double> listD)
        {
            double add = listD.Sum();
            string total = Convert.ToString(add * 1.15);
            return total;
        }


    }
}
