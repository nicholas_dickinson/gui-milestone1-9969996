﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_04_Form
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void subBtn_Click(object sender, EventArgs e)
        {
            string guess = dictionary(guessTxt.Text.ToLower());

            resultTxt.Text = guess;
            resultTxt.Visible = true;
        }

        /*-- Program Logic --*/

        static string dictionary(string guess)
        {

            Dictionary<string, string> newdiction = new Dictionary<string, string>();

            newdiction.Add("apple", "Fruit");
            newdiction.Add("banana", "Fruit");
            newdiction.Add("pear", "Fruit");
            newdiction.Add("tomato", "vegetable");
            newdiction.Add("cucumber", "vegetable");
            newdiction.Add("potato", "vegetable");

            string answer;


            if (newdiction.ContainsKey(guess))
            {

                answer = "Your Guess is Correct!";
                return answer;

            }
            else
            {

                answer = "Wrong! Better luck next time!";
                return answer;
            }


        }

    }

    /*-- End Logic --*/
}

