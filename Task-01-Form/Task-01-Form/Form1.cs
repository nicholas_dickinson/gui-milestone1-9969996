﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_Form
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void subBtn_Click(object sender, EventArgs e)
        {
            string choice = selectBox.Text.ToLower();
            double value = Convert.ToDouble(sizeBox.Text);

            resultTxt.Text = theChosenOne(choice, value);
            resultTxt.Visible = true;
        }

        /*-- Program Logic --*/

        static string theChosenOne(string choice, double value)
        {
           
            string converted;

            switch (choice)
            {
                case "miles":

                    converted = convertMile(value).ToString("#.##KM");
                    return converted;

                case "km":

                    converted = convertKm(value).ToString("#.## Miles");
                    return converted;

                default:

                    return null;
            }
        }




        static double convertKm(double mile)
        {
            double convertedKm = mile * 0.62137119;

            return convertedKm;

        }

        static double convertMile(double km)
        {
            double convertedMile = km / 0.62137119;

            return convertedMile;

        }

        /*-- End Logic --*/

    }
}
