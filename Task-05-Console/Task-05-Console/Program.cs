﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Welcome to the Random Number Guessing Game!\n");
            Console.Write("\nPlease Enter a number between 1 and 5: ");

            int totalScore = checkAnswer();

            Console.WriteLine("\nYour score is {0}", totalScore);
            
        }

    /*-- Program Logic --*/

        static int checkAnswer()
        {

            int amountOfGuesses = 0;
            int answeredCorrectly = 0;

            int correct = randNumber();

            while (amountOfGuesses < 5)
            {

                int guess = guessNumber();

                if (guess == correct)
                {
                    answeredCorrectly++;
                    amountOfGuesses++;

                }
                else {
                    
                   amountOfGuesses ++;
                }

            }

            return answeredCorrectly;  

        }

        static int randNumber()
        {
            
            Random rand = new Random();
            int correctAnswer = rand.Next(1, 5);

            return correctAnswer;
        }

        static int guessNumber()
        {
            int guessMade = Convert.ToInt32(Console.ReadLine());

            return guessMade;
        }




    /*-- End Logic --*/


    }
}
