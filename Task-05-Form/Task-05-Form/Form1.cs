﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_Form
{
    public partial class Form1 : Form
    {
        int correctValue = randNumber();
        int answeredCorrectly;
        int amount = 1;

        public Form1()
        {
            InitializeComponent();

        }

        private void subBtn_Click(object sender, EventArgs e)
        {
            if(amount < 5)
            {
                int guess = Convert.ToInt32(guessBox.Text);
                answeredCorrectly += checkAnswer(guess, correctValue);
                guessBox.Clear();
                amount++;
            }
            else if(amount == 5)
            {
                subBtn.Enabled = false;
                guessBox.Enabled = false;
                MessageBox.Show("Your Final Score is " + answeredCorrectly.ToString());
            }


        }

        /*-- Program Logic --*/

        static int checkAnswer(int guess, int answer)
        {

                if (guess == answer)
                {
                    return 1;
                }
                else { return 0; }


        }

        static int randNumber()
        {
            Random rand = new Random();
            int correctAnswer = rand.Next(1, 5);

            return correctAnswer;
        }

        static int guess()
        {

            return 0;
        }

        /*-- End Logic --*/
    }
}
